function [arGalimaDeti] = arGalimaDeti(lenta, eilute, stulpeliuKiekis)
    arGalimaDeti = true;
    
    % tikrinama eilute
    if(sum(lenta(eilute, :) > 0))
        arGalimaDeti = false;
        return
    end
    
    % tikrinamas stulpelis
    if(sum(lenta(:, stulpeliuKiekis) > 0))
        arGalimaDeti = false;
        return
    end
    
    papildomasStulpelis = stulpeliuKiekis - eilute;
    
    % tikrinama istrizai nuo kairio kampo iki desinio
    if(sum(diag(lenta, papildomasStulpelis) > 0))
        arGalimaDeti = false;
        return
    end
    
    papildomasStulpelis = size(lenta, 2) - stulpeliuKiekis + 1;
    papildomasStulpelis2 = papildomasStulpelis - eilute;
    
    % tikrinama istrizai nuo desinio kampo iki kairio
    if(sum(diag(fliplr(lenta), papildomasStulpelis2) > 0))
        arGalimaDeti = false;
        return
    end 
end