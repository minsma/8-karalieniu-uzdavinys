function [lenta, arPavyko] = deliotiValdoves(lenta, stulpeliuKiekis)
    karalieniuKiekis = size(lenta, 1);
    
    % jeigu isdeliotos visos valdoves grazinamas rezultatas
    if stulpeliuKiekis > karalieniuKiekis
        arPavyko = true;
        return
    end
    
   arGalimaDeliot = false(1, karalieniuKiekis);
   
   for eilute = 1:karalieniuKiekis
       arGalimaDeliot(eilute) = arGalimaDeti(lenta, eilute, stulpeliuKiekis);
   end
   
   % jeigu nera kur galima delioti, o valdoviu dar like, grazinama, 
   % kad nepavyko ivykdyti
   if ~any(arGalimaDeliot)
       arPavyko = false;
       return
   end
   
   % karalieniu deliojimas
   for eilute = 1:karalieniuKiekis
       if arGalimaDeliot(eilute)
           lenta(eilute, stulpeliuKiekis) = 1;
           [lenta, arPavyko] = deliotiValdoves(lenta, stulpeliuKiekis + 1);
           
           if arPavyko
               % pavykus grazinamas rezultatas
               return
           else
               % nepavykus atstatoma buvus reiksme
               lenta(eilute, stulpeliuKiekis) = 0;
           end
       end
   end
   
   % jeigu buvo daeita iki sios eilutes, tuomet programai nepavyko
   % isdelioti valdoviu
   arPavyko = false;
end